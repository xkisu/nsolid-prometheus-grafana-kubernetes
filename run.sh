#!/bin/bash

# Prompt for the N|Solid license token
echo ""
echo "======== NSOLID_LICENSE_TOKEN ========"
echo "The license token can be acquired from running"
echo " $ nsolid-cli license-token"
echo "On a server running a licensed console, or by querying"
echo "the route /api/v3/license-token on the console."
echo ""
read -s -p "> (hidden) " NSOLID_LICENSE_TOKEN
echo ""
echo ""

# Base64 encode the license token and set it as a variable
export NSOLID_LICENSE_TOKEN_BASE64="$(echo -n $NSOLID_LICENSE_TOKEN | base64 -w 0)"

# Prompt for the Docker registry to use
echo "======== Docker Registry ========"
echo "Specify the docker registry to upload the images to."
echo "If using DockerHub, just enter your username, if using GitLab"
echo "enter \"registry.gitlab.com/username/repo\". (NO TRAILING SLASHES)"
echo ""
echo "Ensure you have ran \"docker login\" for your registry first!"
echo ""
read -p "> " DOCKER_REGISTRY
export DOCKER_REGISTRY="${DOCKER_REGISTRY}"
echo ""
echo ""


# Build the docker image
echo "Building docker images..."
docker build -t ${DOCKER_REGISTRY}/nsolid-test-app ./app-image
docker build -t ${DOCKER_REGISTRY}/nsolid-statsd ./nsolid-statsd
echo "Docker images built!"
echo ""
echo "Pushing docker images..."
docker push ${DOCKER_REGISTRY}/nsolid-test-app
docker push ${DOCKER_REGISTRY}/nsolid-statsd
echo "Pushed docker images!"
echo ""

# Create a Kubernetes secret with the license token
echo "Creating k8s N|Solid License Token secret.."
# Substitute the NSOLID_LICENSE_TOKEN_BASE64 variable placeholder in the file with the value
# and then apply the Kubernetes configuration 
envsubst < ./kubernetes/nsolid_license_token.yml | kubectl apply -f -
echo "Created k8s N|Solid secret!"
echo ""

# Deploy the deployment
echo "Deploying.."
envsubst < ./kubernetes/deployment.yml | kubectl apply -f -
echo "Deployed!"
echo ""

# kubectl apply -f ./kubernetes
# envsubst < ./kubernetes/deployment.yml | 