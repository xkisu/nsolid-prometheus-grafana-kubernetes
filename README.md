# Summary

This repository is a proof-of-concept of deploying a NodeJS app running with N|Solid on Kubernetes, accompanied by an instance of the statsd_exporter for Prometheus. The deployment includes Prometheus and Grafana 

First the docker images are built and uploaded to the specified docker registry.

After that the NSolid License token is Base64 encoded and stored in Kubernetes as a secret so the pods can use it as an enviroment variable. 



# Usage

You'll need to have a running Kubernetes cluster online somewhere, along with Kubectl installed on your local machine and configured to connect to the cluster. 

Simply execute `run.sh` and it will walk you through building the images and deploying them to the cluster.


